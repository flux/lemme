# Copyright (C) 2016 by the Free Software Foundation, Inc.
#
# This file is part of GNU Mailman.
#
# GNU Mailman is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# GNU Mailman is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# GNU Mailman.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup

setup(
    name='lemme',
    version='0.1',
    description='The authenticating REST proxy for GNU Mailman',
    author='The Mailman Developers',
    author_email='mailman-developers@python.org',
    license='GPLv3',
    url='http://www.list.org',
    keywords='email',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: '
            'GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Communications :: Email :: Mailing List Servers',
        'Topic :: Communications :: Usenet News',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        ],
    include_package_data=True,
    entry_points={
        'flake8.extension': ['B40 = mailman.testing.flake8:ImportOrder'],
        },
    install_requires=[
        ],
    )
